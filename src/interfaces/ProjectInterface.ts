export default interface ProjectInterface {
    title: string;
    image: string;
    imageAlt: string;
    video?: string;
    description: string;
    link?: string;
};