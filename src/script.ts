const scrollSpy = window.onload = () => {

    /** source : https://grafikart.fr/tutoriels/scrollspy-js-page-491 */

    const spies = document.querySelectorAll('[data-spy]');
    let threshold = window.innerWidth < 1024 ? .6 : .8;


    let observer: IntersectionObserver | null = null;

    /** 
     * @param {HTMLElement} elem 
     */
    const activate = (elem: HTMLElement) => {
        const id = elem.getAttribute('id');
        const anchor = document.querySelector(`a[href="#${id}"]`);

        if (anchor === null) {
            return;
        }

        // ul>li>a
        anchor.parentElement!.parentElement!
            .querySelectorAll('.active')
            .forEach(node => node.classList.remove('active'));

        anchor.classList.add('active');
    }

    /**
     * @param {IntersectionObserverEntry[]} entries 
     */
    const callback = (entries: IntersectionObserverEntry[]) => {
        entries.forEach((entry) => {
            if (entry.isIntersecting) {
                activate(entry.target as HTMLElement);
            }
        })
    };

    /**
     * @param {NodeListOf<Element>} elems 
     */
    const observe = (elems: NodeListOf<Element>) => {
        if (observer !== null) {
            elems.forEach((elem) => observer!.unobserve(elem));
        }

        observer = new IntersectionObserver(callback, {
            threshold: threshold,
        });

        elems.forEach((elem) => {
            observer!.observe(elem);
        });
    }

    /**
     * @param {Function} callback 
     * @param {number} delay 
     * @returns 
     */
    const debounce = (callback: Function, delay: number) => {
        let timer: number;
        return () => {
            // let args = arguments;
            let context = this;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, arguments);
            }, delay);
        }
    }

    if (spies.length > 0) {
        observe(spies);

        let windowH = window.innerHeight;
        let windowW = window.innerWidth;

        window.addEventListener('resize', debounce(() => {
            if (window.innerHeight !== windowH) {
                observe(spies);
                windowH = window.innerHeight;
            } else if (window.innerWidth !== windowW) {
                threshold = window.innerWidth < 1024 ? .6 : .8;
                observe(spies);
                windowW = window.innerWidth;
            }
        }, 500));

    }
}

export default scrollSpy;