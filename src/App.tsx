import React, { useState } from 'react';
// Components
import Author from './components/Author';
import Contact from './components/Contact';
import Footer from './components/Footer';
import MyProjects from './components/MyProjects';
import Nav from './components/Nav';
// Styles
import { GlobalStyle } from './GlobalStyle';
// Script
import scrollSpy from './script';

function App() {
  // const script = useScript('/assets/script.js');

  return (
    <>
      <GlobalStyle />
      <Nav />
      <Author />
      <MyProjects />
      <Contact />
      <Footer />
      { scrollSpy() }
    </>
  );
}

export default App;
