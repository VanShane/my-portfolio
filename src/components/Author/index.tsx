import React from 'react';
// Components
import Title from './Title';
import Portrait from './Portrait';
// Styles
import { Container, Content, Wrapper } from './Author.style';
import About from './About';
import AnimatedBackground from '../Background';

type Props = any;

type State = any;

const Author: React.FC<Props> = (props: Props) => {
    return (
        <Wrapper className="author" id="about" data-spy>
            <AnimatedBackground />
            <Container>
                <Content>
                    <Portrait />
                    <Title />
                </Content>
            </Container>
            <Container>
                <Content>
                    <About />
                </Content>
            </Container>
        </Wrapper>
    );
};

export default Author;