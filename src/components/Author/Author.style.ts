import styled from "styled-components";

const Wrapper = styled.section `
    display: flex;
    flex-wrap: wrap;
    flex-direction: row;
    justify-content: space-evenly;
    width: 100%;
    min-height: 100vh;
    background-color: var(--darkGrey);
    position: relative;
`;

const Container = styled.div `
    z-index: 1;
    max-width: 45%;
    min-width: 320px;
    padding: 1rem 1rem 0 1rem;
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
`;

const Content = styled.div `
    background-color: #161a1dde;
    border-radius: .5rem;
    padding: 1rem;
    width: 100%;
    color: var(--cultured);
`;

export { Wrapper, Container, Content };

/**
 * background: linear-gradient(
0deg,#333333,#e5e5e5,#fff);
    background-size: 150% 150%;
    color: #333333f7;
 */