import React from 'react';
// classnames helper
import cn from 'classnames';
// Custom Hooks
import useTyped, { TypePhase } from '../../../hooks/useTyped';
// Styling
import { Content, Typing } from './About.style';

type Props = any;

let description = ['Autonome', 'Rigoureux', 'Force de proposition', 'Désireux d\'apprendre!'];

const About: React.FC<Props> = (props: Props) => {
    const { typed, currentTyped, phase } = useTyped(description);

    return (
        <>
            <Content className="work-together">
                <h2>Travaillons ensemble</h2>
                <p>
                    Récemment diplômé de mon titre professionnel de développeur web
                    et web mobile auprès de l'Afpa de Balma.
                    Je cherche à poursuivre ma quête : <strong>devenir un développeur à part entière !</strong><br />
                    Etes-vous le recruteur / l'entreprise / l'entrepreneur / la startup,
                    souhaitant travailler à mes côtés ?
                    Où connaisez-vous la personne à ma recherche ?
                    Alors <strong>n'hésitez pas à me contacter</strong>,
                    afin que l'on puisse discuter de vive voix de <strong>notre</strong> prochaine <strong>aventure</strong> !
                </p>
            </Content>
            <Content className="qualities">
                <Typing className={cn({
                    ['end-cursor']: phase !== TypePhase.Deleting,
                    ['blinking']: phase === TypePhase.Pausing
                })}
                    aria-label={currentTyped}>{typed}</Typing>
            </Content>
        </>
    );
}

export default About;