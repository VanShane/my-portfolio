import styled from "styled-components";


const Content = styled.div `
    width: 100%;
    margin-top: 3rem;

    >p {
        line-height: 1.8;
        color: var(--lightGray)
    }

    &.work-together {
        >h2 {
            position: relative;
            &::after {
                content: '';
                position: absolute;
                width: 5rem;
                height: 2px;
                background-color: var(--rubyRed);
                left: 0;
                bottom: -.5rem
            }
        }
    }
`;

const Typing = styled.h3`
    text-align: center;
    min-height: 2rem;
    font-weight: 400;
    font-size: 1.4em;

    &.end-cursor::after {
        content: '|';
        color: var(--rubyRed);
    }

    &.end-cursor.blinking::after {
        animation: blink 1s step-start infinite;
    }

    @keyframes blink {
        50% {
            opacity: 0;
        }
    }
`;

export { Content, Typing };