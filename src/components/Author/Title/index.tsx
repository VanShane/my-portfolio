import React, { useState, useEffect } from 'react';
// Styles
import { H1, H2 } from './Title.style';

type Props = any;

const Title: React.FC<Props> = (props: Props) => {
    return (
        <>
            <H1>Shane Vanhoeck</H1>
            <H2>Développeur web &amp; web mobile</H2>
        </>
    );
}

export default Title;