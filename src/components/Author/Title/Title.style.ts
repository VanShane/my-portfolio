import styled from "styled-components";

const H1 = styled.h1`
    font-weight: 400;
    font-size: 1.7em;
    margin-bottom: .5em;
    text-align: center;
    color: var(--rubyRed);
`;

const H2 = styled.h2`
    font-weight: 700;
    margin-top: .5em;
    text-align: center;
`;

export { H1, H2 };