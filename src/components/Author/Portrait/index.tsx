import React from 'react';
// image
import portrait from '../../../assets/images/portrait.jpg';
// Styles
import { Image, Wrapper } from './Portrait.style';

type Props = any;

const Portrait: React.FC<Props> = (props: Props) => {
    return (
        <Wrapper>
            <Image src={portrait} alt="author-img" />
        </Wrapper>
    );
}

export default Portrait;