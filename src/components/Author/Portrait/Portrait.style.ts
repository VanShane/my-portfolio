import styled from "styled-components";

const Wrapper = styled.div `
    margin: auto;
    width: 150px;
    height: 150px;
    border-radius: 50%;
    overflow: hidden;
    box-shadow: 0 0 10px -5px var(--black);
`;

const Image = styled.img `
    max-width: 100%;
    height: auto;
    display: block;
    border-radius: 0.5rem;
    box-shadow: 0 0 10px -5px var(--black);
`;

export { Wrapper, Image }; 
