import React from 'react';
// Styles
import { Wrapper, Content } from './Nav.style';

type Props = any;

const Nav: React.FC<Props> = (props: Props) => {
    return (
        <Wrapper>
            <Content>
                <li><a href="#about"></a></li>
                <li><a href="#projects"></a></li>
                <li><a href="#contact"></a></li>
            </Content>
        </Wrapper>
    );
}

export default Nav;