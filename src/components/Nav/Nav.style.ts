import styled from "styled-components";

const Wrapper = styled.nav`
    z-index: 4;
    position: fixed;
    height: fit-content;
    right: 0%;
    margin-right: .5rem;
    top: 50%;
`;


const Content = styled.ul`
    list-style-type: none;
    margin: 0;
    padding: 0;

    >li {
        padding: .25rem;

        >a{
            display: block;
            width: .75rem;
            height: .75rem;
            border: 1px solid lightgray;
            box-shadow: 0px 3px 10px -4px var(--black);
            border-radius: 50%;

            &.active {
                background-color: var(--imperialRed);
            }

            @media screen and (max-width: 424px) {
                width: .65rem;
                height: .65rem;
            }
        }
    }
`;

export { Wrapper, Content };