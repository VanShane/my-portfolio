import styled from "styled-components";

const Wrapper = styled.footer ``;

const Content = styled.p `
    margin: 0;
    padding: 1rem;
    text-align: center;
    background-color: var(--darkGrey);
    color: var(--cultured);
`;

export { Wrapper, Content };