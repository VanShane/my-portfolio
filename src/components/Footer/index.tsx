import React from 'react';
// Styles
import { Content, Wrapper } from './Footer.style';

type Props = any;

const Footer: React.FC<Props> = (props: Props) => {
    const year = new Date().getFullYear();
    return (
        <Wrapper>
            <Content>&copy; Shane Vanhoeck | {year}</Content>
        </Wrapper>
    );
}

export default Footer;