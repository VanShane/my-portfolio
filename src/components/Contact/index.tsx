import React from 'react';
// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope, faMapMarkerAlt } from '@fortawesome/free-solid-svg-icons';
import { faLinkedin, faGitlab } from '@fortawesome/free-brands-svg-icons';
import { Wrapper, Container, Content } from './Contact.style';

type Props = any;

type State = any;

const Contact: React.FC<Props> = (props: Props) => {


    return (
        <Wrapper className="contact" id="contact" data-spy>
            <h2>Me contacter</h2>
            <Container className="container">
                <Content className="adress">
                    <h3>Adress</h3>
                    <div>
                        <FontAwesomeIcon icon={faMapMarkerAlt} />
                        <p>
                            35 Route de Toulouse
                            <br />
                            32600 L'Isle-Jourdain
                            <br />
                            Gers (France)
                        </p>
                    </div>
                </Content>
                <Content className="social-media">
                    <h3>Find me on</h3>
                    <ul>
                        <li>
                            <a href="https://linkedin.com/in/shane-vanhoeck/" target="_blank">
                                <FontAwesomeIcon icon={faLinkedin} />
                                linkedin.com/in/shane-vanhoeck/
                            </a>
                        </li>
                        <li>
                            <a href="https://gitlab.com/VanShane" target="_blank">
                                <FontAwesomeIcon icon={faGitlab} />
                                gitlab.com/VanShane
                            </a>
                        </li>
                    </ul>
                </Content>
                <Content className="my-email">
                    <h3>E-mail</h3>
                    <p>
                        <a href="mailto:vanhoeckshane@gmail.com">
                            <FontAwesomeIcon icon={faEnvelope} />
                            vanhoeckshane@gmail.com
                        </a>
                    </p>
                </Content>
            </Container>
        </Wrapper>
    );
}

export default Contact;