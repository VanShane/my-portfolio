import styled from "styled-components";

const Wrapper = styled.section`
    padding: 1rem 0;
    background-color: var(--white);

    >h2 {
        text-align: center;
        position: relative;
        width: fit-content;
        margin: auto;
        margin-top: 1rem;

        &::after {
            content: '';
            position: absolute;
            width: 4.5rem;
            height: 2px;
            background-color: var(--rubyRed);
            left: 0;
            bottom: -.5rem
        }
    }
`;

const Container = styled.div`
    display: flex;
    flex-wrap: wrap;
    justify-content: space-evenly;
    align-items: center;
    margin: auto;
`;

const Content = styled.div`
    padding: 1rem;
    min-width: 320px;
    transition: all ease 1s;

    >div {
        display: flex;
        
        >p {
            margin: 0;
        }

        >svg {
            display: block;
            margin-right: .5rem;
            margin-top: .25rem;
            color: var(--carnelian)
        }
    }

    >h3 {
        color: #161a1ddb;
    }

    >ul {
        list-style-type: none;
        margin: 1rem auto;
        padding: 0;
    }

    ul>li>a, >p>a {
        color: var(--black);
        text-decoration: none;
        transition: all ease .5s;

        &:hover {
            color: var(--carnelian);
        }

        >svg {
            color: var(--carnelian);
            margin-right: .25rem;
        }
    }

    @media screen and (max-width: 702px) {
        transition: all ease 1s;
        margin-right: auto;
    }
`;

export { Wrapper, Container, Content }