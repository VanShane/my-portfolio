import React, { ReactElement } from 'react';
// Styles
import { Cube, Wrapper } from './Background.style';

type Props = any;

const AnimatedBackground: React.FC<Props> = (props: Props) => {

    let cubes: ReactElement[] = [];

    for (let i = 0; i < 6; i++) {
        cubes.push(<Cube key={'cube-' + i} />);
    };

    return (
        <Wrapper>
            { 
                cubes.map(cube => cube)
            }
        </Wrapper>
    )
}

export default AnimatedBackground;