import styled from "styled-components";

const Wrapper = styled.section`
    width: 100%;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    position: relative;
    border-bottom: 2px solid var(--carnelian);
    box-shadow: 0 2px 10px -7px var(--black);
`;

const Header = styled.header `
    position: relative;
    top: 0;
    width: 100%;
    background-color: var(--bloodRed);
    color: white;
    border-bottom: 1px solid white;
    box-shadow: 0 2px 10px -5px var(--black);
    z-index: 3;

    >h2 {
        margin: 0;
        padding: 1rem 2rem;
    }
`;

const Content = styled.div `
    overflow: hidden;
    display: flex;
    flex-direction: column;
    scroll-behavior: smooth;
    margin: auto;
    margin-left: 270px;
    transition: all ease .5s;
    width: calc(100% - 270px);

    @media screen and (max-width: 1023px) {
        transition: all ease .5s;
        margin-left: 0;
        width: 100%;
    }

    
`;

export { Wrapper, Header, Content };