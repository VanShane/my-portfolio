import React, { useState } from 'react';
// Video Player
import ReactPlayer from 'react-player';
// Styles 
import { Content } from './Media.style';
// Interface
import ProjectInterface from '../../../../interfaces/ProjectInterface';

type Props = {
    project: ProjectInterface;
    className: string;
};

const Media: React.FC<Props> = ({ project, className}) => {
    return (
            <Content className="media">
                <img
                    className={className}
                    src={project.image}
                    alt={project.imageAlt}
                />
                <ReactPlayer
                    url={project.video}
                    playing={false}
                    controls={true}
                    loop={false}
                    width='100%'
                    height='100%'
                />
            </Content>
    );
}

export default Media;