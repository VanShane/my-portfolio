import styled from "styled-components";


const Content = styled.div`
    width: 100%;
    max-height: fit-content;
    margin: 2rem auto;
    overflow: hidden;
    border-radius: .5rem;
    box-shadow: 0 1px 10px -6px var(--black);
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    align-items: flex-start;
    justify-content: flex-start;

    >img, >div>video {  
        display: block;
        width: 100%;
    }

    >div {
        margin: auto;
        min-width: 100%;
    }

    >img {
        margin-left: 0;

        &.play-video {
            animation: slide-left .5s ease-in-out forwards;
        }

        &.show-picture {
            animation: slide-right .5s ease-in-out forwards;
        }
    }

    @keyframes slide-left {
        from {
            margin-left: 0;
        }

        to {
            margin-left: -100%;
        }
    }

    @keyframes slide-right {
        from {
            margin-left: -100%;
        }

        to {
            margin-left: 0;
        }
    }
`;

export { Content };