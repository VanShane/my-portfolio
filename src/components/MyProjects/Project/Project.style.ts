import styled from "styled-components";

const Content = styled.div`
    padding: 1rem;
    border-radius: .5rem;
    width: 70%;
    margin: auto;
    animation: appear 1s linear forwards;

    @keyframes appear {
        from {
            opacity: 0;
        }
        to {
            opacity: 1;
        }
    }

    @media screen and (max-width: 1023px) {
        min-width: 275px;
        padding: .75rem;
        width: 92.5%;
    }

    @media screen and (max-width: 767px) {
        width: 90%;
    }
    
    >h3 {
        text-align: right;
    }

    >p {
        white-space: pre-line;
        margin: 2rem auto;
        width: fit-content;

        &.gitlab-link {

            >a {
                transition: all ease .5s;
                width: 2.5rem;
                height: 2.5rem;
                display: flex;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                background-color: var(--lightGray);
                box-shadow: 0px 2px 10px -8px var(--black);

                >svg {
                    width: 100%;
                    height: 50%;
                    margin: auto;
                }

                &:hover {
                    background-color: var(--darkGrey);
                    color: var(--cultured);
                }
            }
        }

    }
`;

const Button = styled.button`
    display: block;
    border: none;
    border-radius: .25rem;
    position: relative;
    top: 0;
    cursor: pointer;
    background-color: var(--carnelian);
    color: white;
    box-shadow: 0 0 10px -5px var(--black);
    width: fit-content;
    padding: .75rem;
    margin: auto;
    margin-bottom: 2rem;
    font-family: 'Orbitron', sans-serif;
    letter-spacing: .1rem;
    font-size: .8em;
    transition: all ease .5s;


    &:hover {
        background-color: var(--imperialRed);
    }
`;

export { Content, Button };