import React, { useState } from 'react';
// Interface
import ProjectInterface from '../../../interfaces/ProjectInterface';
// Styles
import { Content, Button } from './Project.style';
// Components
import Media from './Media';
// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faGitlab } from '@fortawesome/free-brands-svg-icons';

type Props = {
    project: ProjectInterface;
    i: number;
};

const Project: React.FC<Props> = ({ project, i }) => {
    const [isActive, setIsActive] = useState(false);
    const [className, setClassName] = useState('');

    const handleClick = () => {
        setIsActive(!isActive);
        isActive ? setClassName('show-picture') : setClassName('play-video');
    }

    return (
        <Content className="card">
            <h3>{project.title}</h3>
            <Media
                project={project}
                className={className}
            />
            {
                project.video &&
                <Button type="button" onClick={handleClick}>{!isActive ? 'Click to see demo' : 'Return'}</Button>
            }
            <p>{project.description}</p>
            {
                project.link &&
                <p className="gitlab-link">
                    <a href={project.link} target="_blank">
                        <FontAwesomeIcon icon={faGitlab} />
                    </a>
                </p>
            }
        </Content>
    );
}

export default Project;