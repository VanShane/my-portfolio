import React, { useState } from 'react';
// Data
import PROJECTS from '../../data/ProjectData';
// Styles
import { Wrapper, Header, Content } from './MyProjects.style';
// Components
import Project from './Project';
import SideNav from './SideNav';

type Props = any;

const MyProjects: React.FC<Props> = (props: Props) => {
    const [activeLink, setActiveLink] = useState(0);

    const handleLink = (e: React.MouseEvent) => {
        e.preventDefault();
        
        const target = e.target as HTMLElement;
        setActiveLink(Number(target.dataset.link));
    }


    const projects = PROJECTS.map((project, key) => {
        if (key === activeLink) {
            return (
                <Project
                    project={project}
                    i={key}
                    key={"project-" + key}
                />
            )
        }
    });

    return (
        <Wrapper className="my-projects" id="projects" data-spy>
            <Header>
                <h2>Mes projets</h2>
            </Header>
            <SideNav activeLink={activeLink} callback={(e) => handleLink(e)} />
            <Content className="project-container">
                {projects}
            </Content>
        </Wrapper>
    );
}

export default MyProjects;