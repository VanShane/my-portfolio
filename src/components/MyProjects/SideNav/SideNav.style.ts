import styled from "styled-components";

const Aside = styled.aside`
    height: 100%;
    width: 270px;
    position: absolute;
    top: 0;
    left: 0;
    margin-left: 0;
    background-color: var(--darkGrey);
    box-shadow: 2px 2px 10px -6px var(--black);
    z-index: 2;

    @media screen and (min-width: 1024px) {
        animation: showNav .5s linear forwards;
    }

    @media screen and (max-width: 1023px) {
        animation: hideNav .5s linear forwards;

        &.active {
            animation: showNav .5s linear forwards;
        }
    }

    @keyframes hideNav {
        from {
            margin-left: 0;
        }

        to {
            /* margin-left: -300px; */
            margin-left: -265px;
        }
    }

    @keyframes showNav {
        from {
            /* margin-left: -300px; */
            margin-left: -265px;
        }

        to {
            margin-left: 0;
        }
    }
`;

const Nav = styled.nav`
    height: 100%;
    margin-top: 1rem;
    
    >ul {
        height: 100%;
        display: flex;
        flex-direction: column;
        align-items: baseline;
        justify-content: space-evenly;
        list-style-type: none;
        padding: 0 2rem;
        margin: 0;

        >li {
            width: 100%;
            
            >a {
                cursor: pointer;
                position: relative;
                text-decoration: none;
                color: var(--lightGray);
                font-family: 'Orbitron', sans-serif !important;
                transition: all ease .3s;
                width: 100%;
                display: block;

                 &::after {
                    content: '';
                    position: absolute;
                    bottom: -.5rem;
                    left: 0;
                    width: 0;
                    height: 2px;
                    background-color: var(--lightGray);
                    transition: all linear .3s;
                }

                &.active {
                    &::after {
                        background-color: var(--rubyRed);
                    }
                }

                &.active, &:hover {
                    color: var(--cultured);

                    &::after {
                        width: 2.5rem;
                    } 
                }
            }
        }
    }
`;

const Button = styled.button`
    position: absolute;
    top: 50%;
    right: 0;
    margin-right: 0;
    border: none;
    background-color: var(--darkGrey);
    width: 2.5rem;
    height: 2.5rem;
    border-radius: 50%;
    color: white;
    cursor: pointer;
    box-shadow: 2px 2px 10px -6px var(--black);
    visibility: collapse;
    transition: all ease .5s;
    z-index: 1;
    padding: 0;

    >svg {
        margin-left: 1.25rem;
        font-size: .725rem;
        color: var(--transparentWhite);
        visibility: hidden;

        &.fa-minus {
            font-size: .7rem;
        }
    }

    @media screen and (max-width: 1023px) {
        visibility : visible;
        margin-right: -12.5px;

        >svg {
            visibility: visible;
        }
    }
`;

export { Aside, Nav, Button };