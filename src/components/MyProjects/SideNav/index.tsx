import React, { useState } from 'react';
// Styles
import { Aside, Nav, Button } from './SideNav.style';
// Data
import PROJECTS from '../../../data/ProjectData';
// FontAwesome
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus, faMinus } from '@fortawesome/free-solid-svg-icons';

type Props = {
    activeLink: number;
    callback: (e: React.MouseEvent) => void;
};

const SideNav: React.FC<Props> = ({activeLink, callback}) => {
    const [isOpen, setIsOpen] = useState(false);

    const handleClick = () => {
        !isOpen ? setIsOpen(true) : setIsOpen(false);
    }

    const projects = PROJECTS.map((project, key) => (
        <li key={"project-" + key}>
            <a
                data-link={key}
                onClick={callback}
                className={activeLink === key ? 'active' : ''}
            >
                {project.title}
            </a>
        </li>
    ));

    return (
        <Aside className={isOpen ? 'active' : ''}>
            <Nav>
                <ul>
                    {projects}
                </ul>
            </Nav>
            <Button
                onClick={handleClick}>
                {
                    !isOpen ?
                        <FontAwesomeIcon icon={faPlus} />
                        :
                        <FontAwesomeIcon icon={faMinus} />
                }
            </Button>
        </Aside>
    );
}

export default SideNav;