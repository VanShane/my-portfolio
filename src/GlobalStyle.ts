import { createGlobalStyle } from "styled-components";

export const GlobalStyle = createGlobalStyle`
    :root {
        /* --black: #333333;
        --transparentWhite: #ffffffc7;
        --red: #c83232;
        --darkRed: #ab2525; */
        --black: #0b090a;
        --darkGrey: #161a1d;
        --bloodRed: #660708;
        --rubyRed: #a4161a;
        --carnelian: #ba181b;
        --imperialRed: #e5383b;
        --silverChalice: #b1a7a6;
        --lightGray: #d3d3d3;
        --cultured: #f5f3f4;
        --white: #ffffff;
    }

    * {
        box-sizing: border-box;
    }

    html {
        scroll-behavior: smooth;
    }
        
    body {
    margin: 0;
    padding: 0;
    font-size: calc(.75em + .5vmin);
    word-wrap: break-word;
    overflow-x: hidden;
    /** temporary */
    background-color: #d3d3d338;
    }

    h1,
    h2,
    h3 {
        font-family: 'Orbitron', sans-serif;
    }

    h2 {
        color: var(---carnelian);
    }

    p,
    a,
    span {
        font-family: 'Open Sans', sans-serif;
        color: var(--black);
    }

    p {
        text-align: justify;
    }
`;