import ProjectInterface from "../interfaces/ProjectInterface";
// Images
import wildworld from '../assets/images/projects/choixAnimal&Weather.png';
import dynamicFilters from '../assets/images/projects/dynamicFilters_homePage.jpg';
import blog from '../assets/images/projects/symfonyBlog_homePage.jpg';
import html5up from '../assets/images/projects/html5up.jpg';
import calculatorMockup from '../assets/images/projects/calculator_mockup2.jpg';
// Videos 
import wildworldDemo from '../assets/videos/wildworld-demo.mp4';
import filterDemo from '../assets/videos/filter-demo.mp4';
import blogDemo from '../assets/videos/blog-demo.mp4';
import html5upDemo from '../assets/videos/html5up-demo.mp4';

const PROJECTS: ProjectInterface[] = [
    {
        title: 'Terrarium connecté',
        image: wildworld,
        imageAlt: 'image projet terrarium connecté',
        video: wildworldDemo,
        description: `Création de l'espace "Mon Terrarium", composant communiquant avec le microcontrôleur (ESP32).\n
        Projet réalisé en Symfony 5 avec : API-Platform, MySQL, Doctrine, Webpack, JavaScript, Sass, Twig, Bootstrap, diverses librairies JS et packages PHP/Symfony.`,
    },
    {
        title: 'Filtre Produit Symfony',
        image: dynamicFilters,
        imageAlt: 'image projet filtre produit Symfony',
        video: filterDemo,
        description: `Créer une page dynamique avec Symfony 5, PostgreSql, Ajax et Sass.`,
        link: 'https://gitlab.com/VanShane/dynamic-symfony'
    },
    {
        title: 'Blog Symfony',
        image: blog,
        imageAlt: 'image projet blog symfony',
        video: blogDemo,
        description: `Créer un espace administrateur avec EasyAdmin 3, Symfony 5, MySql et Bootstrap.`,
        link: 'https://gitlab.com/VanShane/symfony-blog'
    },
    {
        title: 'Editorial by HTML5UP',
        image: html5up,
        imageAlt: 'image projet html5up',
        video: html5upDemo,
        description: `Recréer une page à partir d'un exemple. Exercice front-end réalisé avec HTML5 et Sass`,
        link: 'https://gitlab.com/VanShane/editorial-html5up'
    },
    {
        title: 'Calculatrice Neumorphisme',
        image: calculatorMockup,
        imageAlt: 'image projet caclulatrice neumorphisme',
        description: `Réaliser une calculatrice en JavaScript avec un design Neumorphisme. Réalisé avec Axentix, NeuAxentix et Sass.`,
        link: 'https://gitlab.com/VanShane/neu'
    }
];

export default PROJECTS;