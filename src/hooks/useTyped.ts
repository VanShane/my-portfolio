import { useState, useEffect } from 'react';

/** source : https://www.youtube.com/watch?v=N4899I-tAW4&ab_channel=JohnnyMagrippis */

export enum TypePhase {
    Typing,
    Pausing,
    Deleting,
};

const TYPING_INTERVAL = 150;
const PAUSE_MS = 1000;
const DELETING_INTERVAL = 50;
const DELETING_PAUSE_MS = 500;

const useTyped = (strings: string[]): {
    typed: string,
    currentTyped: string,
    phase: TypePhase
} => {
    const [selectedIndex, setSelectedIndex] = useState(0);
    const [phase, setPhase] = useState(TypePhase.Typing);
    const [typed, setTyped] = useState('');

    useEffect(() => {
        switch (phase) {
            case TypePhase.Typing: {
                const nextTyped = strings[selectedIndex].slice(
                    0,
                    typed.length + 1
                );

                if (nextTyped === typed) {
                    setPhase(TypePhase.Pausing);
                    return;
                };

                const timeout = setTimeout(() => {
                    setTyped(nextTyped);
                }, TYPING_INTERVAL);

                return () => clearTimeout(timeout);
            }
            case TypePhase.Deleting: {
                if (!typed) {
                    const timeout = setTimeout(() => {
                        const nextIndex = selectedIndex + 1;
                        setSelectedIndex(strings[nextIndex] ? nextIndex : 0);
                        setPhase(TypePhase.Typing);
                    }, DELETING_PAUSE_MS);
                    
                    return () => clearTimeout(timeout);
                };

                const nextRemaining = strings[selectedIndex].slice(
                    0,
                    typed.length - 1
                );

                const timeout = setTimeout(() => {
                    setTyped(nextRemaining);
                }, DELETING_INTERVAL);

                return () => clearTimeout(timeout);
            }
            case TypePhase.Pausing:
            default:
                const timeout = setTimeout(() => {
                    setPhase(TypePhase.Deleting);
                }, PAUSE_MS);

                return () => clearTimeout(timeout);
        }
    }, [strings, typed, phase]);

    return {typed, currentTyped: strings[selectedIndex], phase };
}

export default useTyped;